import React from 'react';
import HomePage from './pages/HomePage/HomePage';
import ProductsPage from './pages/ProductsPage/HomePage';
import About from './pages/About/About';
import LoginPage from './pages/LoginPage/LoginPage';
import SignUpPage from './pages/SignUpPage/SignUpPage';
import PostSalePage from './pages/PostSalePage/PostSalePage';
import ProductDetailPage from './pages/ProductDetailPage/ProductDetailPage';
import WishListPage from './pages/WishListPage/WishListPage';
import ProfilePage from './pages/ProfilePage/ProfilePage';
import { Route, Redirect } from 'react-router-dom';
import ManageProductsPage from './pages/ManageProductsPage/ManageProductsPage';
import ForgotPasswordPage from './pages/ForgotPasswordPage/ForgotPasswordPage';
import ResetPasswordPage from './pages/ResetPasswordPage/ResetPasswordPage';
import ChatPage from './pages/ChatPage/ChatPage';
import LiveStreamPage from './pages/LiveStreamPage/LiveStreamPage';
const routes = [
    {
        path: '/',
        exact: true,
        main: ({ location, history }) => <HomePage location={location} history={history}></HomePage>
    },
    {
        path: '/products/detail/:id',
        exact: true,
        main: ({ location, match, history }) => <ProductDetailPage location={location} match={match} history={history}></ProductDetailPage>
    },
    {
        path: '/profile',
        exact: false,
        main: () => <ProfilePage isPrivated={true}></ProfilePage>
    },
    {
        path: '/about',
        exact: false,
        main: () => <About></About>
    },
    {
        path: '/login',
        exact: true,
        main: ({ history }) => <LoginPage history={history} isRestricted={true}></LoginPage>
    },
    {
        path: '/signup',
        exact: true,
        main: ({ history }) => <SignUpPage history={history} isRestricted={true}></SignUpPage>
    },
    {
        path: '/forgotpassword',
        exact: true,
        main: ({ history }) => <ForgotPasswordPage history={history} isRestricted={true}></ForgotPasswordPage>
    },
    {
        path: '/resetpassword/:tokenreset',
        exact: true,
        main: ({ history, match }) => <ResetPasswordPage match={match} history={history} isRestricted={true}></ResetPasswordPage>
    },
    {
        path: '/postsale',
        exact: true,
        main: () => <PostSalePage isPrivated={true}></PostSalePage>
    },
    {
        path: '/wishlist',
        exact: true,
        main: () => <WishListPage isPrivated={true}></WishListPage>
    },
    {
        path: '/myproducts',
        exact: true,
        main: () => <ManageProductsPage isPrivated={true}></ManageProductsPage>
    },
    {
        path: '/messages/:id?',
        exact: true,
        main: ({ history, match, location }) => <ChatPage match={match} history={history} location={location} />
    },
    {
        path: '/livestreams/:id?',
        exact: true,
        main: ({ history, match, location }) => <LiveStreamPage match={match} history={history} location={location} />
    },
    {
        path: '/:city/:category',
        exact: true,
        main: ({ location, history, match }) => <ProductsPage location={location} history={history} match={match}></ProductsPage>
    },
];

export default routes;