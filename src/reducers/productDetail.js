import * as Types from './../constants/ActionTypes';
import _ from 'lodash';
let initialState = {
    product: {}
};
const myReducer = (state = initialState, action) => {
    switch (action.type) {
        case Types.FETCH_PRODUCT_DETAIL:
            state.product = action.product;
            return { ...state };
        case Types.TOGGLE_LIKE_PRODUCT_DETAIL:
            state.product.hasLiked = !state.product.hasLiked;
            return { ...state };
        default:
            return { ...state };
    }
};

export default myReducer;