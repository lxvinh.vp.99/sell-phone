import { combineReducers } from 'redux';
import auth from './auth';
import products from './products';
import productDetail from './productDetail';
import wishList from './wishList';
import myProducts from './myProducts';
const rootReducer = combineReducers({
    auth,
    products,
    productDetail,
    wishList,
    myProducts
});
export default rootReducer;