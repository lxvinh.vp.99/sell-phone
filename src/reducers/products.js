import * as Types from './../constants/ActionTypes';
import _ from 'lodash';
let initialState = {
    totalProducts: 0,
    products: []
};
const myReducer = (state = initialState, action) => {
    switch (action.type) {
        case Types.FETCH_PRODUCTS:
            state = {
                totalProducts: action.totalProducts,
                products: action.products
            };
            return { ...state };
        case Types.TOGGLE_LIKE_PRODUCT:
            let index = action.index - 1;
            state.products[index]["hasLiked"] = !state.products[index]["hasLiked"];
            return { ...state };
        default:
            return { ...state };
    }
};

export default myReducer;