import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import callApi from '../../helpers/apiCaller';
import { Link } from "react-router-dom";
import * as myProductsActions from '../../actions/myProductsActions';
import Swal from 'sweetalert2';

const ManageProductsItem = (props) => {
    let { product, index } = props;
    const dispatch = useDispatch();
    const onUpdateStatus = (e) => {

        const id = product._id, status = e.currentTarget.getAttribute('updatevalue');
        let title = "", text = "";
        if (status == 2) {
            title = 'Bạn có muốn đổi trạng thái thành đang giao dịch?';
            text = 'Sản phẩm sẽ không xuất hiện trên trang chủ nữa!';
        }
        else if (status == 3) {
            title = 'Bạn có muốn đổi trạng thái thành đã bán?';
            text = 'Sản phẩm sẽ không xuất hiện trên trang chủ nữa!';
        }
        else if (status == 1) {
            title = 'Bạn có muốn đổi trạng thái thành đang bán?';
            text = 'Sản phẩm sẽ xuất hiện trên trang chủ!';
        }
        Swal.fire({
            title: title,
            text: text,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Đồng ý',
            cancelButtonText: 'Hủy bỏ'
        }).then((result) => {
            if (result.value) {
                callApi('products/updatestatus', 'POST', { id: id, status: status }).then(res => {
                    Swal.fire(
                        'Thành công',
                        'Đã đổi trạng thái sản phẩm',
                        'success'
                    )
                    dispatch(myProductsActions.updateStatusMyProducts(id, status));
                }).catch(err => {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: `${err.response.data.message}`,
                    })
                });
            }
        })

    }
    return (
        <React.Fragment>
            <tr>
                <th scope="row">{index}</th>
                <td><Link to={`/products/detail/${product._id}`}>{product && product.title ? product.title : ""}</Link></td>
                {/* <td>{product && product.description ? product.description : ""}</td> */}
                <td>{product && product.oldPrice ? parseInt(product.oldPrice).toLocaleString() : "Không có"}</td>
                <td>{product && product.price ? parseInt(product.price).toLocaleString() : ""}</td>
                <td>{product && product.createdAt ? (new Date(product.createdAt)).toLocaleString() : ""}</td>
                <td>{product && product.status ? product.status == 1 ? <span className="badge badge-info" style={{ cursor: "pointer" }} updatevalue={2} onClick={onUpdateStatus}>Đang bán</span> : product.status == 2 ? <div><p className="badge badge-warning" style={{ cursor: "pointer", margin: 0 + "px" }} updatevalue={3} onClick={onUpdateStatus}>Đang giao dịch</p><p className="badge badge-danger" style={{ cursor: "pointer", margin: 0 + "px" }} updatevalue={1} onClick={onUpdateStatus}>Hủy</p></div> : <span className="badge badge-success" style={{ cursor: "pointer" }}>Đã bán</span> : ""}</td>
                {/* <td>
                    Giao hàng tại số nhà 52, ngách 51, ngõ 155 Cầu Giấy, Hà Nội
                        <span className="badge badge-warning"><i className="fas fa-pen-square"></i></span>
                </td> */}
            </tr>
        </React.Fragment>
    )
}
export default ManageProductsItem;
