import React, { useEffect, useState } from 'react';
import SlideShow from '../SlideShow/SlideShow';
const SliderImages = (props) => {

  let { images } = props;
  let collection = images.map((image, index) => {
    return { src: image, caption: `Ảnh ${index + 1}` };
  });
  // const collection = [
  //   { src: images && images[0] ? images[0] : "", caption: "Caption one" },
  //   { src: images && images[1] ? images[1] : "", caption: "Caption two" },
  // ];
  return (
    <React.Fragment>
      {images && images.length > 0 ? <SlideShow
        input={collection}
        ratio={`3:2`}
        mode={`manual`}
      /> : ""}
    </React.Fragment>
  )
}
export default SliderImages;
