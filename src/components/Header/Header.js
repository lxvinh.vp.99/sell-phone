import React, { useState } from 'react';
import { Link, NavLink as RRNavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Badge
} from 'reactstrap';

const Header = (props) => {
  const auth = useSelector(state => state.auth);
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  const onClick = () => {

  };
  const onLogout = () => {
    localStorage.removeItem("token");
    window.location.href = "/";
  }
  return (
    <div style={{
      position: "sticky",
      top: 0,
      zIndex: 9999,
      backgroundColor: 'rgb(255, 186, 0)',
    }}>
      <Navbar style={{
        maxWidth: '960px',
        margin: 'auto',
        backgroundColor: 'rgb(255, 186, 0)',
      }}
        expand="md"
      >
        <NavbarBrand href="/" style={{
          color: 'black',
        }}>CHỢ ONLINE</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink tag={RRNavLink} exact to="/" activeClassName="active" style={{
                color: 'black',
              }}><i className="fas fa-home"></i> Trang chủ</NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={RRNavLink} exact to="/livestreams" activeClassName="active" style={{
                color: 'black',
              }}><i className="fas fa-headset"></i> Livestream</NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={RRNavLink} exact to="/messages" activeClassName="active" style={{
                color: 'black',
              }}><i className="fas fa-comment-dots"></i> Chat</NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={RRNavLink} exact to="/postsale" activeClassName="active" style={{
                color: 'black',
              }}><i className="fas fa-edit"></i> Đăng bán</NavLink>
            </NavItem>
            {auth.isLogin ?
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret style={{
                  color: 'black',
                }}>
                  {auth.userInfo && auth.userInfo.firstName && auth.userInfo.lastName ? auth.userInfo.firstName + " " + auth.userInfo.lastName : ""}
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    <NavLink tag={RRNavLink} exact to="/profile/myinfo" className="active" style={{
                      color: 'black',
                    }}>Thông tin của tôi</NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink tag={RRNavLink} exact to="/myproducts" className="active" style={{
                      color: 'black',
                    }}>Quản lý sản phẩm đăng bán</NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink tag={RRNavLink} exact to="/wishlist" activeClassName="active" style={{
                      color: 'black',
                    }}>Yêu thích <Badge color="danger">{auth.userInfo && auth.userInfo.totalWishProducts ? auth.userInfo.totalWishProducts : 0}</Badge></NavLink>
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem >
                    <NavLink onClick={onLogout} className="active">Đăng xuất</NavLink>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown> :
              <NavItem>
                <NavLink tag={RRNavLink} exact to="/login" activeClassName="active" style={{
                  color: 'black',
                }}
                >Đăng nhập</NavLink>
              </NavItem>}
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}
export default Header;
