import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import callApi from '../../helpers/apiCaller';
import * as productActions from '../../actions/productActions';
import ProductCard from '../ProductCard/ProductCard';
const ProductFrame = (props) => {
  const dispatch = useDispatch();
  const { products } = useSelector(state => state.products);
  const [title, setTitle] = useState("");
  useEffect(() => {
    callApi(`products/list-top`, 'POST', null).then(res => {
      setTitle(res.data.data.title);
      dispatch(productActions.fetchProducts(res.data.data.totalProducts, res.data.data.products));
    }).catch(err => {
      console.log(err.response.data.message);
    });
  }, []);
  let elmProducts = [];
  if (products.length > 0) {
    elmProducts = products.map((product, index) => {
      return <ProductCard {...props} key={product._id} index={index + 1} product={product} />
    });
  }
  else {
    elmProducts = <div style={{ height: 60 + "vh", width: 100 + "%", display: "flex", alignItems: "center", justifyContent: "center" }}><h3 style={{ textAlign: "center" }}>Chưa có sản phẩm nào được đăng bán</h3></div>
  }
  return (
    <React.Fragment>
      <div class="GridAds__Wrapper">
        <p class="GridAds__Title">{title}</p>
        <div class="GridAds__Grid">
          {elmProducts}
        </div>
      </div>
    </React.Fragment>
  )
}
export default ProductFrame;
