import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import callApi from '../../helpers/apiCaller';
import * as authActions from '../../actions/authActions';
import { Redirect } from "react-router-dom";
const withAuth = (WrappedComponent) => props => {
    const dispatch = useDispatch();
    const { isPrivated, isRestricted } = props;
    const [checkLogin, setCheckLogin] = useState(false);
    const [isProcess, setIsProcess] = useState(true);
    useEffect(() => {
        callApi('users/myinfo', 'POST', null).then(res => {
            dispatch(authActions.authUser(true, res.data.data));
            setCheckLogin(true);
        }).catch(err => {
            // console.log(err.response.data.message);
            dispatch(authActions.authUser(false, {}));
            setCheckLogin(false);
        }).finally(() => {
            setIsProcess(false);
        });
    }, []);
    if (isProcess) {
        return <div style={{ height: 100 + "vh" }}></div>
    }
    else {
        if (isPrivated && !checkLogin) {
            return <Redirect to="/login" />
        }
        else if (!isPrivated && isRestricted && checkLogin) {
            return <Redirect to="/" />
        }
        return <WrappedComponent {...props} />;
    }

};

export default withAuth;