import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import callApi from '../../helpers/apiCaller';
import * as productDetailActions from '../../actions/productDetailActions';
import SliderImages from '../SliderImages/SliderImages';
import './style.scss';
import _ from 'lodash';
import Swal from 'sweetalert2';
import { toast } from 'react-toastify';
function useStateCallback(initialState) {
  const [state, setState] = useState(initialState);
  const cbRef = useRef(null);
  const setStateCallback = (state, cb) => {
    cbRef.current = cb;
    setState(state);
  };
  useEffect(() => {
    if (cbRef.current) {
      cbRef.current(state);
      cbRef.current = null;
    }
  }, [state]);
  return [state, setStateCallback];
}
const ProductDetailFrame = (props) => {
  let { id } = props.match.params;
  const dispatch = useDispatch();
  const { product } = useSelector(state => state.productDetail);
  const [isProcess, setIsProcess] = useStateCallback(false);
  useEffect(() => {
    callApi(`products/detail/${id}`, 'POST', null).then(res => {
      if (res.data.data && res.data.data.product) {
        dispatch(productDetailActions.fetchProductDetail(res.data.data.product));
      }
      else {
        dispatch(productDetailActions.fetchProductDetail({}));
      }
    }).catch(err => {
      console.log(err.response.data.message);
    });
  }, []);
  const onBack = () => {
    if (props.location.state && props.location.state.back) {
      props.history.push(props.location.state.back);
    }
    else {
      props.history.push("/products?page=1&size=6");
    }
  };
  const onToggleLike = (e) => {
    e.preventDefault();
    if (!isProcess) {
      setIsProcess(true, () => {
        let payload = { "productId": product._id }
        let endpoint = product && product.hasLiked ? "wishlists/remove" : "wishlists/add";
        let type = product && product.hasLiked ? "remove" : "add";
        callApi(endpoint, 'POST', payload).then(res => {
          toast.dismiss();
          if (product.hasLiked) {
            toast.error("👌 Đã xóa khỏi danh sách yêu thích");
          }
          else {
            toast.success("👌 Đã thêm vào danh sách yêu thích");
          }
          dispatch(productDetailActions.toggleLikeProductDetail(type));
          setIsProcess(false);
        }).catch(err => {
          if (err && err.response) {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: "Bạn phải đăng nhập để thực hiện chức năng này",
            });
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: "Có lỗi xảy ra vui lòng thử lại",
            });
          }
          setIsProcess(false);
        });
      });
    }
  }

  const createGroupChat = () => {
    callApi('create-chat', 'POST', { "productId": product._id }).then(res => {
      // console.log(props.history);
      props.history.push(`/messages/${res.data.data}`);
    }).catch(err => {
      console.log(err.response.data.message);
    });
  }

  return (
    <React.Fragment>
      <div className="container product-detail-wrapper" style={{ marginTop: 20 + "px" }}>
        <button type="button" className="btn btn-primary" style={{ marginBottom: 20 + "px" }} onClick={onBack}><i className="fas fa-long-arrow-alt-left"></i> Quay lại</button>
        {!_.isEmpty(product) ? <div className="row" style={{ backgroundColor: "white" }}>
          <div className="col-12 col-sm-12 col-md-8">
            <SliderImages images={product && product.images ? product.images : []}></SliderImages>
            <div>
              <div>
                <h3>{product && product.title ? product.title : ""}</h3>
                {product && !product.hasLiked ?
                  <button type="button" className="btn btn-outline-danger" onClick={onToggleLike}>Yêu thích <i className="far fa-heart"></i></button> :
                  <button type="button" className="btn btn-danger" onClick={onToggleLike}>Bỏ thích <i className="far fa-heart"></i></button>}
              </div>
              <h4 style={{ color: "#c90927" }}><small style={{ textDecoration: "line-through", marginRight: 5 + "px", color: "#9b9b9b" }}>{product && product.oldPrice ? parseInt(product.oldPrice).toLocaleString() : ""}</small>{product && product.price ? `${parseInt(product.price).toLocaleString()} VNĐ` : ""} </h4>
              <p style={{ whiteSpace: "pre-line" }}>{product && product.description ? product.description : ""}</p>
            </div>
          </div>
          <div className="col-12 col-sm-12 col-md-4 seller-wrapper">
            <div className="seller-name-wrapper">
              <i className="far fa-user" style={{ width: 24 + "px", height: 24 + "px" }}></i>
              <p>{product && product.sellerFullName ? product.sellerFullName : ""}</p>
            </div>
            {/* <div className="seller-email-wrapper">
              <i className="fas fa-envelope-square" style={{ width: 24 + "px", height: 24 + "px" }}></i>
              <p>{product && product.sellerEmail ? product.sellerEmail : ""}</p>
            </div> */}
            <div className="seller-phone-wrapper">
              <i className="fas fa-phone-square-alt" style={{ width: 24 + "px", height: 24 + "px" }}></i>
              <p>{product && product.sellerPhoneNumber ? product.sellerPhoneNumber : ""}</p>
            </div>
            <div className="seller-address-wrapper">
              <i className="fas fa-address-card" style={{ width: 24 + "px", height: 24 + "px" }}></i>
              <p>{product && product.sellerAddress ? product.sellerAddress : ""}</p>
            </div>
            {/* <div className="seller-createdAt-wrapper">
              <i className="fas fa-clock" style={{ width: 24 + "px", height: 24 + "px" }}></i>
              <p>{product && product.createdAt ? (new Date(product.createdAt)).toLocaleString() : ""}</p>
            </div> */}
            <div className="seller-createdAt-wrapper" onClick={createGroupChat}>
              <i className="fas fa-comment-dots" style={{ width: 24 + "px", height: 24 + "px" }}></i>
              <p>Chat với người bán</p>
            </div>
          </div>
        </div> : <div className="text-error-wrapper"><p className="text-error">Không tìm thấy sản phẩm hoặc sản phẩm đã được bán</p></div>}
      </div>
    </React.Fragment>
  )
}
export default ProductDetailFrame;
