import React, { useEffect, useState } from 'react';
import callApi from '../../helpers/apiCaller';
import { toast } from 'react-toastify';
const ResetPasswordForm = (props) => {
    const [email, setEmail] = useState("");
    const [errorVerify, setErrorVerify] = useState(null);
    const [resetInfo, setResetInfo] = useState({ password: "", confirmPassword: "" });
    const onChange = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        setResetInfo({ ...resetInfo, [name]: value });
    }
    const [isProcessIntinial, setIsProcessIntinial] = useState(true);
    let { tokenreset } = props.match.params;
    useEffect(() => {
        callApi(`users/verify/${tokenreset}`, 'POST', null).then(res => {
            setEmail(res.data.data.email);
        }).catch(err => {
            setErrorVerify(err.response.data.message);
        }).finally(() => {
            setIsProcessIntinial(false);
        });
    }, []);
    const onReset = (e) => {
        e.preventDefault();
        callApi(`users/resetpassword/${tokenreset}`, 'POST', resetInfo).then(res => {
            toast.dismiss();
            toast.success(`${res.data.response.msg}`, {
                onClose: () => {
                    props.history.push('/login', { email: email, password: resetInfo.password });
                },
                autoClose: 1000
            });
            setResetInfo({});
        }).catch(err => {
            toast.dismiss();
            toast.error(`${err.response.data.message}`);
        });
    }
    return (
        <React.Fragment>
            {isProcessIntinial ? <div style={{ height: 60 + "vh" }}></div> :
                errorVerify ? errorVerify :
                    <form>
                        <h3>Khởi tạo lại mật khẩu</h3>
                        <div className="form-group">
                            <label>Email</label>
                            <input type="email"
                                className="form-control"
                                placeholder="Nhập email"
                                value={email ? email : ""}
                                name="email"
                                disabled
                            />
                        </div>
                        <div className="form-group">
                            <label>Mật khẩu</label>
                            <input type="password"
                                className="form-control"
                                placeholder="Nhập mật khẩu"
                                name="password"
                                onChange={onChange}
                                value={resetInfo && resetInfo.password ? resetInfo.password : ""}
                            />
                        </div>
                        <div className="form-group">
                            <label>Xác nhận mật khẩu</label>
                            <input type="password"
                                className="form-control"
                                placeholder="Xác nhận mật khẩu"
                                name="confirmPassword"
                                onChange={onChange}
                                value={resetInfo && resetInfo.confirmPassword ? resetInfo.confirmPassword : ""}
                            />
                        </div>
                        <button type="submit" className="btn btn-primary btn-block" onClick={onReset}>Đổi mật khẩu</button>
                    </form>}
        </React.Fragment>
    )
}
export default ResetPasswordForm;
