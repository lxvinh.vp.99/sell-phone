import React from 'react';
const Pagination = (props) => {
  let { currentPage, totalPage } = props;
  const onHandleNextPage = (e) => {
    e.preventDefault();
    let crtPage = currentPage;
    if (e.target.getAttribute("pageindex") === "-1") {
      crtPage--;
    }
    else if (e.target.getAttribute("pageindex") === "+1") {
      crtPage++;
    }
    else {
      crtPage = e.target.getAttribute("pageindex");
    }
    props.onHandleNextPage(crtPage);
  }
  let items = [];
  for (let i = 1; i <= totalPage; i++) {
    items.push(<li className={currentPage && (i == currentPage) ? "page-item active" : "page-item"} key={i} ><a className="page-link" href="#" pageindex={i} onClick={onHandleNextPage}>{i}</a></li>);
  }
  return (
    <React.Fragment>
      <nav aria-label="Page navigation example">
        <ul className="pagination justify-content-center">
          <li className={currentPage == 1 || !currentPage ? "page-item disabled" : "page-item"}>
            <a className="page-link" href="#" pageindex="-1" onClick={onHandleNextPage}>Previous</a>
          </li>
          {items}
          <li className={currentPage == totalPage || !currentPage ? "page-item disabled" : "page-item"}>
            <a className="page-link" href="#" onClick={onHandleNextPage} pageindex="+1">Next</a>
          </li>
        </ul>
      </nav>
    </React.Fragment>
  )
}
export default Pagination;
