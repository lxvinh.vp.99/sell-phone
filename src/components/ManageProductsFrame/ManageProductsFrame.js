import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import callApi from '../../helpers/apiCaller';
import { Table } from 'reactstrap';
import ManageProductsItem from '../ManageProductsItem/ManageProductsItem';
import * as myProductsActions from '../../actions/myProductsActions';
const ManageProductsFrame = (props) => {
    const dispatch = useDispatch();
    const products = useSelector(state => state.myProducts);
    useEffect(() => {
        callApi('products/myproducts', 'POST', null).then(res => {
            dispatch(myProductsActions.fetchMyProducts(res.data.data));
        }).catch(err => {
            console.log(err.response.data.message);
        });
    }, []);
    let elmProducts = [];
    if (products.length > 0) {
        elmProducts = products.map((product, index) => {
            return <ManageProductsItem key={product._id} index={index + 1} product={product} />
        });
    }
    return (
        <React.Fragment>
            <Table responsive bordered striped style={{ textAlign: "center" }}>
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tiêu đề</th>
                        {/* <th>Mô tả</th> */}
                        <th>Giá cũ</th>
                        <th>Giá</th>
                        <th>Đăng lúc</th>
                        <th>Trạng thái</th>
                        {/* <th>Ghi chú</th> */}
                    </tr>
                </thead>
                <tbody>
                    {elmProducts}
                </tbody>
            </Table>
        </React.Fragment>
    )
}
export default ManageProductsFrame;
