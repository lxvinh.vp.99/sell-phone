import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Breadcrumb, BreadcrumbItem, ButtonDropdown, DropdownToggle, Modal, ModalHeader, ModalBody, ListGroup, ListGroupItem } from 'reactstrap';
import { Link } from "react-router-dom";
import { get } from 'lodash';
import callApi from '../../helpers/apiCaller';
import * as productActions from '../../actions/productActions';
import Pagination from '../Pagination/Pagination';
import ProductCard from '../ProductCard/ProductCard';
const queryString = require('query-string');
const ProductFrame = (props) => {
  let query = queryString.parse(props.location.search);
  const dispatch = useDispatch();
  const { totalProducts, products } = useSelector(state => state.products);
  const [breadcrumb, setBreadcrumb] = useState([]);
  const [listCity, setListCity] = useState([]);
  const [listCategory, setListCategory] = useState([]);
  const [isOpenModalSelectCity, setIsOpenModalSelectCity] = useState(false);
  const [isOpenModalSelectCategory, setIsOpenModalSelectCategory] = useState(false);
  const [selectedCity, setSelectedCity] = useState("");
  const [selectedCategory, setSelectedCategory] = useState("");
  let totalPage = Math.ceil(totalProducts / 10);
  useEffect(() => {
    const { match } = props;
    const { city, category } = match.params;
    callApi(`category/list`, 'GET', null).then(res => {
      const { data } = res.data;
      setListCategory(data);
      const item = data.find((itemCity) => itemCity.sID === category);
      const selectedCategory = get(item, 'name') || 'Tất cả';
      setSelectedCategory(selectedCategory);
    }).catch(err => {
      console.log(err.response.data.message);
    });
    callApi(`city/list`, 'GET', null).then(res => {
      const { data } = res.data;
      setListCity(data);
      const item = data.find((itemCity) => itemCity.sID === `/${city}`);
      const selectedCity = get(item, 'name') || 'Toàn quốc';
      setSelectedCity(selectedCity);
    }).catch(err => {
      console.log(err.response.data.message);
    });
  }, [])
  useEffect(() => {
    const { match, location } = props;
    const { params } = match;
    callApi(`products/list/${params.city}/${params.category}${location.search}`, 'POST', null).then(res => {
      dispatch(productActions.fetchProducts(res.data.data.totalProducts, res.data.data.products));
      setBreadcrumb(res.data.data.breadcrumb);
    }).catch(err => {
      console.log(err.response.data.message);
    });
  }, [props.location]);
  const onHandleNextPage = (pageindex) => {
    query.page = pageindex;
    let search = queryString.stringify(query);
    props.history.push({ search: search });
  }
  let elmProducts = [];
  if (products.length > 0) {
    elmProducts = products.map((product, index) => {
      return <ProductCard {...props} key={product._id} index={index + 1} product={product} />
    });
  }
  else {
    elmProducts = <div style={{ height: 60 + "vh", width: 100 + "%", display: "flex", alignItems: "center", justifyContent: "center" }}><h3 style={{ textAlign: "center" }}>Chưa có sản phẩm nào được đăng bán</h3></div>
  }

  const renderBreadcrumb = () => {
    return (
      <Breadcrumb listClassName="wrapper__wrapperBreadcrumb">
        <BreadcrumbItem>
          <Link
            to={{
              pathname: '/',
            }}
          >
            Trang chủ
          </Link>
        </BreadcrumbItem>
        {
          breadcrumb.map((item) => {
            return (
              <BreadcrumbItem key={item.sID} active={item.active}>
                {!item.active ? (
                  <Link
                    to={{
                      pathname: item.url,
                    }}
                  >
                    {item.name}
                  </Link>
                ) : item.name}
              </BreadcrumbItem>
            )
          })
        }
      </Breadcrumb>
    )
  }

  const renderFilter = () => {
    const { match, history } = props;
    const { category: selectedsIDCategory, city: selectedsIDCity } = match.params;
    return (
      <div className="wrapper__wrapperFilter">
        <ButtonDropdown toggle={() => { setIsOpenModalSelectCity(true) }} className="wrapper__wrapperButtonFilter">
          <DropdownToggle caret>
            <i className="fas fa-map-marker-alt"></i> {selectedCity}
          </DropdownToggle>
        </ButtonDropdown>
        <ButtonDropdown toggle={() => { setIsOpenModalSelectCategory(true) }} className="wrapper__wrapperButtonFilter">
          <DropdownToggle caret>
            <i className="fas fa-list-ul"></i> {selectedCategory}
          </DropdownToggle>
        </ButtonDropdown>
        <Modal isOpen={isOpenModalSelectCity} toggle={() => { setIsOpenModalSelectCity(false) }} style={{ marginTop: '50px' }}>
          <ModalHeader toggle={() => { setIsOpenModalSelectCity(false) }}>Chọn khu vực</ModalHeader>
          <ModalBody style={{ maxHeight: '500px', overflow: 'auto' }}>
            <ListGroup>
              {
                listCity.map((city) => {
                  return (
                    <ListGroupItem
                      style={{ textAlign: 'left' }}
                      tag="button"
                      onClick={() => {
                        setSelectedCity(city.name);
                        history.push(`${city.sID}/${selectedsIDCategory}`);
                        setIsOpenModalSelectCity(false);
                      }}
                    >{city.name}</ListGroupItem>
                  )
                })
              }
            </ListGroup>
          </ModalBody>
        </Modal>
        <Modal isOpen={isOpenModalSelectCategory} toggle={() => { setIsOpenModalSelectCategory(false) }} style={{ marginTop: '50px' }}>
          <ModalHeader toggle={() => { setIsOpenModalSelectCategory(false) }}>Chọn thể loại</ModalHeader>
          <ModalBody style={{ maxHeight: '500px', overflow: 'auto' }}>
            <ListGroup>
              {
                listCategory.map((category) => {
                  return (
                    <ListGroupItem
                      style={{ textAlign: 'left' }}
                      tag="button"
                      onClick={() => {
                        setSelectedCategory(category.name);
                        history.push(`/${selectedsIDCity}/${category.sID}`);
                        setIsOpenModalSelectCategory(false);
                      }}
                    >{category.name}</ListGroupItem>
                  )
                })
              }
            </ListGroup>
          </ModalBody>
        </Modal>
      </div>
    )
  }

  return (
    <React.Fragment>
      <div class="GridAds__Wrapper">
        {renderBreadcrumb()}
        {renderFilter()}
        <p class="GridAds__Title" style={{ textTransform: 'capitalize' }}>{`${selectedCategory} ${selectedCity}`}</p>
        <div class="GridAds__Grid">
          {elmProducts}
        </div>
      </div>
      <div style={{ marginTop: '20px' }}>
        <Pagination totalPage={totalPage} currentPage={query.page || 1} onHandleNextPage={onHandleNextPage}></Pagination>
      </div>
    </React.Fragment>
  )
}
export default ProductFrame;
