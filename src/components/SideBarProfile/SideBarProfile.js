import React from 'react';
import { ListGroup, ListGroupItem } from 'reactstrap';
import { NavLink as RRNavLink } from 'react-router-dom';
import './style.scss';
const SideBarProfile = () => {
    return (
        <ListGroup className="menu-option">
            <ListGroupItem tag={RRNavLink} to="/profile/myinfo" action><i className="far fa-user"></i> Thông tin</ListGroupItem>
            <ListGroupItem tag={RRNavLink} to="/profile/changepassword" action><i className="fas fa-key"></i> Mật khẩu</ListGroupItem>
        </ListGroup>
    )
}
export default SideBarProfile
