import React from 'react';
import { StyledWrapperCategory } from './styled';
import callApi from '../../helpers/apiCaller';
import { Link } from 'react-router-dom';

class CategoriesHomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listCategory: [],
    };
  }

  componentDidMount() {
    callApi(`category/list`, 'GET', null).then(res => {
      const { data } = res.data;
      this.setState({
        listCategory: data,
      })
    }).catch(err => {
      console.log(err.response.data.message);
    });
  }

  render() {
    const { listCategory } = this.state;

    return (
      <React.Fragment>
        <StyledWrapperCategory className="Categories__Wrapper" >
          <p class="Categories__Title">Khám phá danh mục</p>
          <div>
            <div className="Wrapper__WrapperCategories">
              {listCategory.map(ctg => (
                <li key={ctg._id}>
                  <Link
                    to={{
                      pathname: `/toan-quoc/${ctg.sID}`
                    }}
                  >
                    <img src={ctg.image} alt="img" />
                    <span>{ctg.name}</span>
                  </Link>
                </li>
              ))}
            </div>
          </div>
        </StyledWrapperCategory>
      </React.Fragment >
    )
  }
}

export default CategoriesHomePage;
