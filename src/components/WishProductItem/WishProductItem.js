import React, { useState, useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from "react-router-dom";
import callApi from '../../helpers/apiCaller';
import Swal from 'sweetalert2';
import * as wishListActions from '../../actions/wishListActions';
import { toast } from 'react-toastify';
function useStateCallback(initialState) {
    const [state, setState] = useState(initialState);
    const cbRef = useRef(null);
    const setStateCallback = (state, cb) => {
        cbRef.current = cb;
        setState(state);
    };
    useEffect(() => {
        if (cbRef.current) {
            cbRef.current(state);
            cbRef.current = null;
        }
    }, [state]);
    return [state, setStateCallback];
}
const WishProductItem = (props) => {
    const { product } = props;
    const [isProcess, setIsProcess] = useStateCallback(false);
    const dispatch = useDispatch();
    const onRemoveWishProduct = (e) => {
        e.preventDefault();
        if (!isProcess) {
            setIsProcess(true, () => {
                let payload = { "productId": product._id }
                callApi('wishlists/remove', 'POST', payload).then(res => {
                    toast.dismiss();
                    toast.error("👌 Đã xóa khỏi danh sách yêu thích");
                    setIsProcess(false);
                    dispatch(wishListActions.removeWishProduct(product._id));
                }).catch(err => {
                    if (err && err.response) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: "Bạn phải đăng nhập để thực hiện chức năng này",
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: "Có lỗi xảy ra vui lòng thử lại",
                        });
                    }
                    setIsProcess(false);
                });
            });
        }
    }
    return (
        <React.Fragment>
            <Link className="link-product-detail" to={"/products/detail/" + product._id}>
                <div className="wishlist-item-wrapper">
                    <div className="img-wrapper">
                        <img src={product && product.images ? product.images[0] : ""} alt="anh" className="img-product" />
                    </div>
                    <div className="info-wrapper">
                        <h3>{product && product.title ? product.title : ""}</h3>
                        <p className="product-price"><small>{product && product.oldPrice ? parseInt(product.oldPrice).toLocaleString() : ""}</small>{parseInt(product.price).toLocaleString()} VNĐ</p>
                        <p className="like-icon" onClick={onRemoveWishProduct}><i className="fa fa-heart"></i></p>
                    </div>
                </div>
            </Link>
        </React.Fragment >
    )
}
export default WishProductItem;
