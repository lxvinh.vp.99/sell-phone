import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import callApi from '../../helpers/apiCaller';
import { Button, Form, FormGroup, Label, Input, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap';
import { toast } from 'react-toastify';
function useStateCallback(initialState) {
  const [state, setState] = useState(initialState);
  const cbRef = useRef(null);

  const setStateCallback = (state, cb) => {
    cbRef.current = cb;
    setState(state);
  };
  useEffect(() => {
    if (cbRef.current) {
      cbRef.current(state);
      cbRef.current = null;
    }
  }, [state]);
  return [state, setStateCallback];
}
const PostSaleForm = () => {
  const [imagePreviewUrls, setImagePreviewUrls] = useState([]);
  const [productInfo, setProductInfo] = useStateCallback({});
  const [listCategory, setListCategory] = useState([]);
  const [listCity, setListCity] = useState([]);
  // const [sellerInfo, setSellerInfo] = useState({});
  const [files, setFiles] = useState([]);
  const { isLogin, userInfo } = useSelector(state => state.auth);
  useEffect(() => {
    callApi(`category/list`, 'GET', null).then(res => {
      const { data } = res.data;
      setListCategory(data);
    }).catch(err => {
      console.log(err.response.data.message);
    });
    callApi(`city/list`, 'GET', null).then(res => {
      const { data } = res.data;
      setListCity(data);
    }).catch(err => {
      console.log(err.response.data.message);
    });
  }, []);
  useEffect(() => {
    setProductInfo({
      sellerEmail: userInfo && userInfo.email ? userInfo.email : "",
      sellerFullName: userInfo ? userInfo.firstName + " " + userInfo.lastName : "",
      sellerPhoneNumber: userInfo && userInfo.phoneNumber ? userInfo.phoneNumber : "",
      sellerAddress: userInfo && userInfo.address ? userInfo.address : ""
    });
  }, [userInfo]);
  const onAddImages = (e) => {
    if (e.target.files) {
      const files = Array.from(e.target.files);
      Promise.all(files.map(file => {
        setFiles()
        return (new Promise((resolve, reject) => {
          const reader = new FileReader();
          reader.addEventListener('load', (ev) => {
            resolve(ev.target.result);
          });
          reader.addEventListener('error', reject);
          reader.readAsDataURL(file);
        }));
      }))
        .then(images => {
          setImagePreviewUrls(images);
          setFiles(files);
        }, error => {
          console.error(error);
        });
    }
  }
  const onSubmitForm = (e) => {
    e.preventDefault();
    const formData = new FormData();
    files.forEach(file => {
      formData.append("many-files", file);
    });
    for (const property in productInfo) {
      formData.append(property, productInfo[property]);
    }
    callApi('products/create', 'POST', formData).then(res => {
      setImagePreviewUrls([]);
      setFiles([]);
      setProductInfo({
        ...productInfo,
        title: "",
        description: "",
        price: "",
        city: "",
        category: "",
      });
      toast.dismiss();
      toast.success("👌 Đăng bán sản phẩm thành công");
    }).catch(err => {
      toast.dismiss();
      toast.error(`👌 ${err.response.data.message}`);
    });
  }
  const handleOnChange = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    setProductInfo({ ...productInfo, [name]: value });
  }
  // const handleOnChangeSellerInfo = (e) => {
  //   let name = e.target.name;
  //   let value = e.target.value;
  //   setSellerInfo({ ...sellerInfo, [name]: value });
  // }
  let elmImages = [];
  if (imagePreviewUrls.length > 0) {
    elmImages = imagePreviewUrls.map((url, index) => {
      return <div key={index + 1} index={index + 1} style={{ backgroundImage: `url(${url})`, backgroundSize: "100% 100%" }} className="select-images" />
    });
  }
  return (
    <React.Fragment>
      <div className="container" style={{ marginTop: 20 + "px", marginBottom: 20 + "px" }}>
        <Form onSubmit={onSubmitForm}>
          <div className="row">
            <div className="col-12 col-sm-12 col-md-8">
              <h2>Thông tin sản phẩm</h2>
              <div className="images-wrapper">
                {elmImages}
                <label htmlFor="files" className="select-images">
                  <i className="fas fa-camera" style={{ width: 50 + "%", height: 50 + "%", transform: "translate(50%, 50%)", color: "orange" }}></i>
                  <i className="fas fa-plus" style={{ color: "orange", position: "absolute", right: 5 + "px", top: 5 + "px" }}></i>
                </label>
                <input type="file" id="files" name="files" multiple style={{ display: "none" }} onChange={onAddImages} />
              </div>
              <FormGroup>
                <Label htmlFor="title">Tiêu đề</Label>
                <Input type="text" name="title" id="title" value={productInfo && productInfo.title ? productInfo.title : ""} onChange={handleOnChange} />
              </FormGroup>
              <FormGroup>
                <Label htmlFor="description">Mô tả</Label>
                <Input type="textarea" name="description" id="description" value={productInfo && productInfo.description ? productInfo.description : ""} onChange={handleOnChange} />
              </FormGroup>
              <FormGroup>
                <Label htmlFor="category">Thể loại</Label>
                <Input
                  type="select"
                  name="category"
                  id="category"
                  value={productInfo && productInfo.category ? productInfo.category : ""}
                  onChange={handleOnChange}
                >
                  <option value=""></option>
                  {listCategory.map((category) => {
                    return (
                      <option key={category._id} value={category._id}>{category.name}</option>
                    )
                  })}
                </Input>
              </FormGroup>
              <FormGroup>
                <Label htmlFor="price">Giá</Label>
                <InputGroup>
                  <Input
                    type="number"
                    name="price"
                    id="price"
                    value={productInfo && productInfo.price ? productInfo.price : ""}
                    onChange={handleOnChange}
                  />
                  <InputGroupAddon addonType="append">
                    <InputGroupText>VNĐ</InputGroupText>
                  </InputGroupAddon>
                </InputGroup>
              </FormGroup>
            </div>
            <div className="col-12 col-sm-12 col-md-4">
              <h2>Địa chỉ người bán</h2>
              <FormGroup>
                <Label htmlFor="sellerFullName">Họ tên</Label>
                <Input type="text" name="sellerFullName" id="sellerFullName" value={productInfo && productInfo.sellerFullName ? productInfo.sellerFullName : ""} onChange={handleOnChange} />
              </FormGroup>
              <FormGroup>
                <Label htmlFor="sellerPhoneNumber">Số điện thoại</Label>
                <Input type="text" name="sellerPhoneNumber" id="sellerPhoneNumber" value={productInfo && productInfo.sellerPhoneNumber ? productInfo.sellerPhoneNumber : ""} onChange={handleOnChange} />
              </FormGroup>
              <FormGroup>
                <Label htmlFor="sellerEmail">Email</Label>
                <Input
                  type="email"
                  name="sellerEmail"
                  id="sellerEmail"
                  value={productInfo && productInfo.sellerEmail ? productInfo.sellerEmail : ""}
                  disabled
                />
              </FormGroup>
              <FormGroup>
                <Label htmlFor="city">Khu vực</Label>
                <Input
                  type="select"
                  name="city"
                  id="city"
                  value={productInfo && productInfo.city ? productInfo.city : ""}
                  onChange={handleOnChange}
                >
                  <option value=""></option>
                  {listCity.map((city) => {
                    return (
                      <option key={city._id} value={city._id}>{city.name}</option>
                    )
                  })}
                </Input>
              </FormGroup>
              <FormGroup>
                <Label htmlFor="sellerAddress">Địa chỉ</Label>
                <Input type="text" name="sellerAddress" id="sellerAddress" value={productInfo && productInfo.sellerAddress ? productInfo.sellerAddress : ""} onChange={handleOnChange} />
              </FormGroup>
            </div>
          </div>
          <Button color="success" style={{ margin: "auto", display: "block" }}>Đăng bán</Button>
        </Form>
      </div>
    </React.Fragment>
  )
}
export default PostSaleForm;
