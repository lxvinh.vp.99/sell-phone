import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import callApi from '../../helpers/apiCaller';
const ProductItem = (props) => {
    const product = props.product;
    return (
        <React.Fragment>
            <div className="el-wrapper">
                <div className="box-up">
                    <img className="img" src={product.images[0]} alt="" />
                    <div className="img-info">
                        <div className="info-inner">
                            <span className="p-name">{product.title}</span>
                            {/* <span className="p-company">Yeezy</span> */}
                        </div>
                        <div className="a-size">
                            <p className="content">{product.description}</p>
                            {/* <span className="size">S , M , L , XL</span> */}
                            <Link to={"/products/detail/" + product._id}>View details</Link>
                        </div>
                    </div>
                </div>
                <div className="box-down">
                    <div className="h-bg">
                        <div className="h-bg-inner"></div>
                    </div>
                    <a className="cart" href="#">
                        <span className="price">{product.price} VNĐ</span>
                        <span className="add-to-cart">
                            <span className="txt">Add in cart</span>
                        </span>
                    </a>
                </div>
            </div>
        </React.Fragment >
    )
}
export default ProductItem;
