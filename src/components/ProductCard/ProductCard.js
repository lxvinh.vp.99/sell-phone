import React, { useState, useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from "react-router-dom";
import callApi from '../../helpers/apiCaller';
import Swal from 'sweetalert2';
import './style.scss';
import * as productActions from '../../actions/productActions';
import { toast } from 'react-toastify';
import { convertTime } from '../../helpers/utils';

function useStateCallback(initialState) {
	const [state, setState] = useState(initialState);
	const cbRef = useRef(null);
	const setStateCallback = (state, cb) => {
		cbRef.current = cb;
		setState(state);
	};
	useEffect(() => {
		if (cbRef.current) {
			cbRef.current(state);
			cbRef.current = null;
		}
	}, [state]);
	return [state, setStateCallback];
}
const ProductCard = (props) => {
	const { product, index } = props;
	const [isProcess, setIsProcess] = useStateCallback(false);
	const dispatch = useDispatch();
	const onToggleLike = (e) => {
		e.preventDefault();
		if (!isProcess) {
			setIsProcess(true, () => {
				let payload = { "productId": product._id }
				let endpoint = product && product.hasLiked ? "wishlists/remove" : "wishlists/add";
				let type = product && product.hasLiked ? "remove" : "add";
				callApi(endpoint, 'POST', payload).then(res => {
					toast.dismiss();
					if (product.hasLiked) {
						toast.error("👌 Đã xóa khỏi danh sách yêu thích");
					}
					else {
						toast.success("👌 Đã thêm vào danh sách yêu thích");
					}
					dispatch(productActions.toggleLikeProduct(index, type));
					setIsProcess(false);
				}).catch(err => {
					if (err && err.response) {
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: "Bạn phải đăng nhập để thực hiện chức năng này",
						});
					} else {
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: "Có lỗi xảy ra vui lòng thử lại",
						});
					}
					setIsProcess(false);
				});
			});
		}
	}
	return (
		<React.Fragment>
			{/* <div className="product-card">
                {product && product.createdAt && (Date.now() - (new Date(product.createdAt)).getTime()) / 1000 <= 60 * 60 ? <div className="badge">NEW</div> : ""}
                <div className="product-tumb">
                    <img src={product && product.images ? product.images[0] : ""} alt="" />
                </div>
                <div className="product-details">
                    <h4><Link to={{
                        pathname: `/products/detail/${product._id}`,
                        state: { 'back': props.location.pathname + props.location.search }
                    }}>{product.title}</Link></h4>
                    <p>{product.description}</p>
                    <div className="product-bottom-details">
                        <div className="product-price"><small>{product && product.oldPrice ? parseInt(product.oldPrice).toLocaleString() : ""}</small>{parseInt(product.price).toLocaleString()} VNĐ</div>
                        <div className="product-links">
                            <a href="" className={product.hasLiked ? "active" : ""} onClick={onToggleLike}><i className="fa fa-heart"></i></a>
                        </div>
                    </div>
                </div>
            </div> */}
			<div class="AdItem__Item">
				<div class="wrapperLink">
					<div class="thumbnailWrapper">
						<Link to={{
							pathname: `/products/detail/${product._id}`,
							state: { 'back': props.location.pathname + props.location.search }
						}}
							className="thumbnailImg"
						>
							<img
								alt={product && product.images ? product.images[0] : ""}
								src={product && product.images ? product.images[0] : ""}
								loading="lazy"
							/>
						</Link>
						<div class="saveAdWrapper saveAd">
							<button id="btn_save_ad" type="button" class="btnSavedAd" onClick={onToggleLike}>
								<img
									width="20"
									src={product.hasLiked ? "https://static.chotot.com/storage/chotot-icons/next/save-ad-active.svg" : "https://static.chotot.com/storage/chotot-icons/next/save-ad.svg"}
									alt="like"
								/>
							</button>
						</div>
					</div>
					<div class="caption">
						<Link
							to={{
								pathname: `/products/detail/${product._id}`,
								state: { 'back': props.location.pathname + props.location.search }
							}}
							class="adTitleGrid"
						>
							{product.title}
						</Link>
						<div><span class="price">{parseInt(product.price).toLocaleString()} đ</span></div>
					</div>
					<div class="footerItem">
						<a rel="noopener noreferrer" target="_blank"
						><img
								class="image___3OzO2"
								width="15"
								src="https://static.chotot.com/storage/chotot-icons/next/pro.svg"
								alt="Bán chuyên"
							/></a>
						<div class="deviderWrapper"></div>
						<div class="adItemPostedTime">
							<span class="text">{convertTime((Date.now() - (new Date(product.createdAt)).getTime()) / 1000)}</span>
						</div>
						<div class="deviderWrapper"></div>
						<span class="adItemPostedTime location"
						><span class="text">{product.sellerAddress}</span></span>
					</div>
				</div>
			</div>
		</React.Fragment >
	)
}
export default ProductCard;
