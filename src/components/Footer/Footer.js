import React from 'react';
import './Footer.scss';
import logo from '../../assets/logo.svg';
import twitter from '../../assets/twitter.svg';
import github from '../../assets/github.svg';
import facebook from '../../assets/facebook.svg';
import dribbble from '../../assets/dribbble.svg';
const Footer = () => {
    return (
        <div className="footer-wrapper">
            <footer className="footer">
                <div className="footer__logo"><img src={logo} alt="" />CHỢ ONLINE</div>
                <div className="footer__links">
                    <a href="" className="footer__link">Terms of Service</a>
                    <a href="" className="footer__link">Privacy Policy</a>
                    <a href="" className="footer__link">Security</a>
                    <a href="" className="footer__link">Site map</a>
                </div>
                <div className="footer__social">
                    {/* <select name="" id="">
                        <option value="English">English</option>
                        <option value="Vietnam">Vietnam</option>
                    </select> */}
                    <div className="footer__icons">
                        <div className="footer__icon"><img src={twitter} alt="" /></div>
                        <div className="footer__icon"><img src={github} alt="" /></div>
                        <div className="footer__icon"><img src={facebook} alt="" /></div>
                        <div className="footer__icon"><img src={dribbble} alt="" /></div>
                    </div>
                    <div className="footer__copyright">© 2021 All rights reserved.</div>
                </div>
            </footer>
        </div>
    );
}
export default Footer;
