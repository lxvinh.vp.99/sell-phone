import TLSSigAPIv2 from 'tls-sig-api-v2';

export const convertTime = (time) => {
  const days = ~~(time / 86400)
  const hrs = ~~(time / 3600);
  const mins = ~~((time % 3600) / 60);
  const secs = ~~time % 60;
  if (days) {
    return `${days} ngày trước`;
  }
  if (hrs) {
    return `${hrs} giờ trước`;
  }
  if (mins) {
    return `${mins} phút trước`;
  }
  return `${secs} giây trước`
}

export const generateUserSig = (sdkAppId, secretKey, userId) => {
  const api = new TLSSigAPIv2.Api(sdkAppId, secretKey);
  return api.genSig(userId, 86400 * 180);
}