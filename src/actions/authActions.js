import * as Types from './../constants/ActionTypes';

export const authUser = (isLogin, userInfo) => {
    return {
        type: Types.AUTH_USER,
        isLogin,
        userInfo
    }
}

export const editUser = (userInfo) => {
    return {
        type: Types.EDIT_USER,
        userInfo
    }
}