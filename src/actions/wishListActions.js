import * as Types from './../constants/ActionTypes';
export const fetchWishList = (products) => {
    return {
        type: Types.FETCH_WISHLIST,
        products
    }
}
export const removeWishProduct = (id) => {
    return {
        type: Types.REMOVE_WISH_PRODUCT,
        id
    }
}
