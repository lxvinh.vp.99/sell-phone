import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import SideBarProfile from '../../components/SideBarProfile/SideBarProfile';
import ChangePasswordForm from "../../components/ChangePasswordForm/ChangePasswordForm";
import ChangeInfoForm from "../../components/ChangeInfoForm/ChangeInfoForm";
import withAuth from '../../components/withAuth/withAuth';
const routes = [
  {
    path: "/profile/myinfo",
    exact: true,
    sidebar: () => <SideBarProfile active={1}></SideBarProfile>,
    main: () => <ChangeInfoForm></ChangeInfoForm>
  },
  {
    path: "/profile/changepassword",
    exact: true,
    sidebar: () => <SideBarProfile active={2}></SideBarProfile>,
    main: () => <ChangePasswordForm></ChangePasswordForm>
  }
];

const ProfilePage = () => {
  return (
    <Router>
      <div className="container" style={{ marginBottom: 30 + "px", marginTop: 20 + "px" }}>
        <div className="row">
          <div className="col-3">
            <Switch>
              {routes.map((route, index) => (
                <Route
                  key={index}
                  path={route.path}
                  exact={route.exact}
                  children={<route.sidebar />}
                />
              ))}
            </Switch>
          </div>
          <div className="col-9">
            <Switch>
              {routes.map((route, index) => (
                <Route
                  key={index}
                  path={route.path}
                  exact={route.exact}
                  children={<route.main />}
                />
              ))}
            </Switch>
          </div>
        </div>
      </div>
    </Router>
  );
}

export default withAuth(ProfilePage);