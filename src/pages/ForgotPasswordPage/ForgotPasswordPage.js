import React, { useEffect } from 'react';
import { ToastContainer, Zoom } from 'react-toastify';
import withAuth from '../../components/withAuth/withAuth';
import ForgotPasswordForm from '../../components/ForgotPasswordForm/ForgotPasswordForm';
const ForgotPasswordPage = (props) => {
    return (
        <React.Fragment>
            <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                transition={Zoom}
            />
            <div className="auth-wrapper">
                <div className="auth-inner">
                    <ForgotPasswordForm></ForgotPasswordForm>
                </div>
            </div>
        </React.Fragment>
    )
}
export default withAuth(ForgotPasswordPage);
