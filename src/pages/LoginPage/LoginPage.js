import React, { useEffect } from 'react';
import LoginForm from '../../components/LoginForm/LoginForm';
import './LoginPage.scss';
import { ToastContainer, Zoom } from 'react-toastify';
import withAuth from '../../components/withAuth/withAuth';
const LoginPage = (props) => {
    return (
        <React.Fragment>
            <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                transition={Zoom}
            />
            <div className="auth-wrapper">
                <div className="auth-inner">
                    <LoginForm {...props}></LoginForm>
                </div>
            </div>
        </React.Fragment>
    )
}
export default withAuth(LoginPage);
