import React, { useState } from 'react';
import './PostSalePage.scss';
import withAuth from '../../components/withAuth/withAuth';
import PostSaleForm from '../../components/PostSaleForm/PostSaleForm';
import { ToastContainer, Slide } from 'react-toastify';
const PostSalePage = (props) => {
  return (
    <React.Fragment>
      <ToastContainer
        style={{ top: 56 + "px" }}
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        transition={Slide}
      />
      <PostSaleForm></PostSaleForm>
    </React.Fragment>
  )
}
export default withAuth(PostSalePage);
