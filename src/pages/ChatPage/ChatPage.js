import React from 'react';
import TIM from 'tim-js-sdk';
import { get, isEmpty } from 'lodash';
import "@chatscope/chat-ui-kit-styles/dist/default/styles.min.css";
import {
  MainContainer,
  ChatContainer,
  MessageList,
  Message,
  MessageInput,
  Sidebar,
  Conversation,
  ConversationHeader,
  ConversationList,
  Avatar,
} from "@chatscope/chat-ui-kit-react";
import withAuth from '../../components/withAuth/withAuth';
import { connect } from 'react-redux';
import { generateUserSig } from '../../helpers/utils';
import callApi from '../../helpers/apiCaller';
import { SDKAPPID, SECRETKEY, IM_LOG_LEVEL_NONE } from '../../constants/Config';
import { StyledChat } from './styled';

class ChatPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      conversationList: [],
      myInfo: {},
      selectedConversation: {},
      messageList: [],
      chatText: '',
      isSDKReady: false,
      nextReqMessageID: '',
    };
    const options = {
      SDKAppID: SDKAPPID,
    };
    this.tim = TIM.create(options);
    this.tim.setLogLevel(IM_LOG_LEVEL_NONE);
    this.subscribeEventIM();
  }

  async componentDidMount() {
    const { authData } = this.props;
    if (!authData.isLogin) return;
    const userID = get(authData, 'userInfo.userId').toString();
    try {
      const userSig = generateUserSig(SDKAPPID, SECRETKEY, userID);
      await this.tim.login({ userID, userSig });
    } catch (error) {
      console.log(error);
    }
  }

  async componentWillUnmount() {
    try {
      this.cancelEventIM();
      await this.tim.logout();
    } catch (error) {
      console.log(error);
    }
  }

  componentDidUpdate(prevProps) {
    const { location } = this.props;
    const { conversationList } = this.state;
    if (location.pathname !== prevProps.location.pathname) {
      const arr = location.pathname.split('/');
      const selectedConversation = conversationList.find((cvs) => cvs.groupID === arr[2]);
      this.setState({
        selectedConversation: selectedConversation || {},
      }, () => {
        this.getMessageList();
      });
    }
  }

  getGroupList = async () => {
    try {
      const imRes = await this.tim.getGroupList();
      const { authData, match } = this.props;
      if (!authData.isLogin) return;
      const userID = get(authData, 'userInfo.userId').toString();
      const { groupList } = imRes.data;
      const conversationList = await Promise.all(groupList.map(async (group) => {
        const imRes = await this.tim.getGroupProfile({ groupID: group.groupID });
        const imRes1 = await this.tim.getGroupMemberList({ groupID: group.groupID, count: 2, offset: 0 });
        const imRes2 = await this.tim.getMessageList({ conversationID: `GROUP${group.groupID}`, count: 1 });
        const lastMessage = get(imRes2, 'data.messageList[0]');
        let isRead = false;
        if (!lastMessage || lastMessage.from === "administrator") {
          isRead = true;
        } else {
          isRead = lastMessage.isRead;
        }
        const { memberList } = imRes1.data;
        const otherInfo = memberList.find((member) => member.userID !== userID);
        return {
          groupID: group.groupID,
          introduce: imRes.data.group.introduction,
          avatar: otherInfo.avatar,
          nick: otherInfo.nick,
          isRead,
        };
      }));
      const selectedConversation = conversationList.find((cvs) => cvs.groupID === match.params.id);
      this.setState({
        conversationList,
        selectedConversation: selectedConversation || {},
      }, () => {
        this.getMessageList();
      });
    } catch (error) {
      console.log(error);
    }
  }

  getMyProfile = async () => {
    try {
      const imRes = await this.tim.getMyProfile();
      this.setState({
        myInfo: imRes.data,
      });
    } catch (error) {
      console.log(error);
    }
  }

  getMessageList = async () => {
    const { selectedConversation } = this.state;
    if (!selectedConversation.groupID) return;
    const imRes = await this.tim.getMessageList({
      conversationID: `GROUP${selectedConversation.groupID}`,
      count: 15,
    });
    const { messageList } = imRes.data;
    this.setState({
      messageList,
    });
  }

  sendMessage = async () => {
    const { selectedConversation, chatText, messageList } = this.state;
    let message = this.tim.createTextMessage({
      to: selectedConversation.groupID,
      conversationType: TIM.TYPES.CONV_GROUP,
      payload: {
        text: chatText,
      },
    });
    try {
      const imRes = await this.tim.sendMessage(message);
      const { message: sendMessage } = imRes.data;
      this.setState({
        chatText: '',
        messageList: [...messageList, sendMessage],
      });
    } catch (error) {
      console.log(error);
    }
  }

  handleChangeInput = (value) => {
    this.setState({
      chatText: value,
    });
  }

  onSDKReady = async () => {
    console.log('SDK ready to send message');
    this.setState({
      isSDKReady: true,
    });
    this.getGroupList();
    this.getMyProfile();
  }

  onSDKNotReady = () => {
    this.setState({
      isSDKReady: false,
    });
    console.log('SDK not ready, please try again');
  }

  onMessageReceived = (evt) => {
    const { messageList, selectedConversation, conversationList } = this.state;
    evt.data.forEach((message) => {
      console.log(message);
      if (selectedConversation.groupID === message.to) {
        messageList.push(message);
      } else {
        const conversationIndex = conversationList.findIndex((cvs) => cvs.groupID === message.to);
        conversationList[conversationIndex].isRead = false;
      }
    });
    this.setState({
      messageList: [...messageList],
      conversationList,
    });
  }

  subscribeEventIM = () => {
    this.tim.on(TIM.EVENT.SDK_READY, this.onSDKReady);
    this.tim.on(TIM.EVENT.SDK_NOT_READY, this.onSDKNotReady);
    this.tim.on(TIM.EVENT.MESSAGE_RECEIVED, this.onMessageReceived);
  }

  cancelEventIM = () => {
    this.tim.off(TIM.EVENT.SDK_READY, this.onSDKReady);
    this.tim.off(TIM.EVENT.SDK_NOT_READY, this.onSDKNotReady);
    this.tim.off(TIM.EVENT.MESSAGE_RECEIVED, this.onMessageReceived);
  }

  renderConversationList = () => {
    const { conversationList } = this.state;
    const { history, match } = this.props;
    return (
      <ConversationList>
        {
          conversationList.map((conversation) => {
            return <Conversation
              key={conversation.groupID}
              name={conversation.nick}
              info={conversation.introduce}
              active={conversation.groupID === match.params.id}
              unreadDot={!conversation.isRead}
              onClick={() => history.push(`/messages/${conversation.groupID}`)}
            >
              <Avatar src={conversation.avatar} />
            </Conversation>;
          })
        }
      </ConversationList>
    );
  }

  renderConversationHeader = () => {
    const { myInfo } = this.state;
    return <ConversationHeader style={{ backgroundColor: "#fff" }}>
      <Avatar src={myInfo.avatar} />
      <ConversationHeader.Content>
        {myInfo.nick}
      </ConversationHeader.Content>
    </ConversationHeader>
  }

  renderConversationHeaderRight = () => {
    const { selectedConversation } = this.state;
    return (
      <ConversationHeader>
        <Avatar src={selectedConversation.avatar} />
        <ConversationHeader.Content userName={selectedConversation.nick} />
      </ConversationHeader>
    )
  }

  renderMessageList = () => {
    const { messageList } = this.state;
    return (<MessageList
    >
      {
        messageList.map((message) => {
          return message.from !== "administrator" && (
            <Message
              model={{
                message: message.payload.text,
                sentTime: "just now",
                sender: message.from,
                direction: message.flow === "in" ? "incoming" : "outgoing",
                position: "single"
              }}
              key={message.ID}
            >
              {message.flow === "in" && <Avatar src={message.avatar || "https://chatscope.io/storybook/react/static/media/joe.641da105.svg"} name={message.from} />}
            </Message>
          );
        })
      }
    </MessageList>);
  }

  render() {
    const { chatText, isSDKReady, selectedConversation } = this.state;
    return (
      <React.Fragment>
        <StyledChat className="container">
          <MainContainer responsive>
            <Sidebar position="left" scrollable>
              {this.renderConversationHeader()}
              {this.renderConversationList()}
            </Sidebar>
            {!isEmpty(selectedConversation) && (
              <ChatContainer>
                {this.renderConversationHeaderRight()}
                {this.renderMessageList()}
                <MessageInput
                  value={chatText}
                  onChange={this.handleChangeInput}
                  attachButton={false}
                  placeholder="Viết tin nhắn..."
                  disabled={!isSDKReady}
                  onSend={this.sendMessage}
                />
              </ChatContainer>
            )}
          </MainContainer>
        </StyledChat>
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  authData: state.auth,
});

export default withAuth(connect(mapStateToProps, null)(ChatPage));
