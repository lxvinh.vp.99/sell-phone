import React, { PureComponent } from 'react';
import _ from 'lodash';
import { Badge } from 'reactstrap';
import { Link } from "react-router-dom";

class LiveStreamItem extends PureComponent {
  render() {
    const { liveInfo } = this.props;
    return (
      <React.Fragment>
        <article className="video-container">
          <Link
            to={{
              pathname: `/livestreams/${liveInfo._id}`,
            }}
            className="thumbnail"
          >
            <Badge color="danger" style={{
              position: 'absolute',
              top: '10px',
              left: '10px',
              fontSize: '14px'
            }}>Trực tiếp</Badge>
            <img class="thumbnail-image" src={liveInfo.thumnails[0]} alt="thumnail" />
          </Link>
          <div class="video-bottom-section">
            <Link
              to={{
                pathname: `/livestreams/${liveInfo._id}`,
              }}
            >
              <img class="channel-icon" src="https://static.chotot.com/storage/chotot-icons/svg/user.svg" alt="thumnail" />
            </Link>
            <div class="video-details">
              <Link
                to={{
                  pathname: `/livestreams/${liveInfo._id}`,
                }}
                className="video-title"
              >
                {`LIVE: ${_.get(liveInfo, 'product.title')}`}
              </Link>
              <Link
                to={{
                  pathname: `/livestreams/${liveInfo._id}`,
                }}
                className="video-channel-name"
              >
                {_.get(liveInfo, 'product.sellerFullName')}
              </Link>
              {/* <div class="video-metadata">
                <span>12K views</span>
              •
              <span>1 week ago</span>
              </div> */}
            </div>
          </div>
        </article>
      </React.Fragment >
    );
  }
}

export default LiveStreamItem;
