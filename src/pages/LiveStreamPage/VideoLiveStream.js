import React from 'react';
import TRTC from 'trtc-js-sdk';
import { Badge } from 'reactstrap';
import { get } from 'lodash';
import { generateUserSig } from '../../helpers/utils';
import { SDKAPPID, SECRETKEY } from '../../constants/Config';
import callApi from '../../helpers/apiCaller';

class VideoLiveStream extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      numberOfViewer: 0,
    };
    this.client = null;
    this.localStream = null;
    this.remoteStream = null;
    TRTC.Logger.setLogLevel(TRTC.Logger.LogLevel.NONE);
    this.init();
  }

  componentWillUnmount() {
    this.leaveRoom();
    let roleLS = 'audience';
    const { authData, liveStreamInfo } = this.props;
    const userID = get(authData, 'userInfo._id').toString();
    if (liveStreamInfo.user === userID) roleLS = 'anchor';
    if (roleLS === 'anchor') {
      callApi(`livestreams/end`, 'POST', { roomID: liveStreamInfo._id }).then(() => {
      }).catch(err => {
        console.log(err);
      });
    }
  }

  leaveRoom = async () => {
    if (this.localStream) {
      this.localStream.stop();
      this.localStream.close();
      this.client.unpublish(this.localStream)
        .then(() => { })
        .catch(() => { });
    }
    if (this.remoteStream) {
      this.remoteStream.stop();
      this.remoteStream.close();
      this.client.unsubscribe(this.remoteStream)
        .then(() => { })
        .catch(() => { });
    }
    this.unSubscribeEventTRTC();
    this.client && await this.client.leave();
    this.client = null;
  }

  init = () => {
    let { authData } = this.props;
    if (!authData.isLogin) return;
    const userID = get(authData, 'userInfo.userId').toString();
    const userSig = generateUserSig(SDKAPPID, SECRETKEY, userID);
    this.client = TRTC.createClient({
      mode: 'rtc',
      sdkAppId: SDKAPPID,
      userId: userID,
      userSig,
      useStringRoomId: true,
    });
    this.subscribeEventTRTC();
    this.joinRoomLiveStream();
  }

  joinRoomLiveStream = async () => {
    try {
      let roleLS = 'audience';
      const { authData, match, liveStreamInfo } = this.props;
      const userID = get(authData, 'userInfo._id').toString();
      const { id: roomID } = match.params;
      if (liveStreamInfo.user === userID) roleLS = 'anchor';
      await this.client.join({
        roomId: roomID.toString(),
        role: roleLS,
      });
      if (roleLS === "anchor") {
        this.createLocalStream();
      }
      console.log('Join Room Live Stream Success');
    } catch (error) {
      console.log(error);
    }
  }

  createLocalStream = async () => {
    try {
      const { authData } = this.props;
      const userID = get(authData, 'userInfo.userId').toString();
      this.localStream = TRTC.createStream({
        audio: true,
        video: true,
        // microphoneId: settingLive.microphoneId,
        // cameraId: settingLive.cameraId,
        userId: userID,
        mirror: true,
      });
      await this.localStream.initialize();
      await this.client.publish(this.localStream);
      const id = this.localStream.getId();
      this.addVideoView(id);
      this.localStream.play(id, { muted: true, objectFit: 'contain' })
        .then(() => { })
        .catch(() => { })
        .finally(() => { });
    } catch (error) {
      console.log('Init live stream error: ', error);
    }
  }

  addVideoView = (id) => {
    const div = document.createElement('div');
    const containerElement = document.getElementById('VideoLSContainer');
    div.setAttribute('id', id);
    div.setAttribute('class', 'wrapper-ls');
    containerElement && containerElement.appendChild(div);
  }

  removeVideoView = (id) => {
    const div = document.getElementById(id);
    div && div.remove();
  }

  handleStreamAdded = (evt) => {
    const remoteStream = evt.stream;
    this.client && this.client.subscribe(remoteStream);
  }

  handleStreamSubscribed = (evt) => {
    try {
      this.remoteStream = evt.stream;
      const id = this.remoteStream.getId();
      this.addVideoView(id);
      this.remoteStream.play(id)
        .then(() => { })
        .catch(async (e) => {
          const errorCode = e.getCode();
          if (errorCode === 0x4043) {
            await this.remoteStream.resume();
          }
        })
        .finally(() => { });
    } catch (error) {
      console.log('Error:', error);
    }
  }

  handleStreamRemoved = (evt) => {
    const remoteStream = evt.stream;
    const id = remoteStream.getId();
    this.removeVideoView(id);
  }

  handlePeerJoin = (e) => {
    const { numberOfViewer } = this.state;
    this.setState({
      numberOfViewer: numberOfViewer + 1,
    });
  }

  handlePeerLeave = (e) => {
    const { numberOfViewer } = this.state;
    this.setState({
      numberOfViewer: numberOfViewer - 1,
    });
  }

  subscribeEventTRTC = () => {
    this.client.on('stream-added', this.handleStreamAdded);
    this.client.on('stream-subscribed', this.handleStreamSubscribed);
    this.client.on('stream-removed', this.handleStreamRemoved);
    this.client.on('peer-join', this.handlePeerJoin);
    this.client.on('peer-leave', this.handlePeerLeave);
  }

  unSubscribeEventTRTC = () => {
    this.client.off('stream-added', this.handleStreamAdded);
    this.client.off('stream-subscribed', this.handleStreamSubscribed);
    this.client.off('stream-removed', this.handleStreamRemoved);
    this.client.off('peer-join', this.handlePeerJoin);
    this.client.off('peer-leave', this.handlePeerLeave);
  }

  render() {
    const { numberOfViewer } = this.state;

    return (
      <React.Fragment>
        <div className="wrapper__wrapperVideoLS">
          <div id="VideoLSContainer" />
          <div className="badge__badgeLive">
            <Badge color="danger">Trực tiếp</Badge>
            <Badge color="secondary" style={{
              backgroundColor: 'rgba(0, 0, 0, 0.5)',
              marginLeft: '5px',
            }}><i className="fas fa-eye"></i> {numberOfViewer}</Badge>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default VideoLiveStream;