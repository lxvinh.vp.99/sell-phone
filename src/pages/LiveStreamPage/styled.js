import styled from 'styled-components';

export const StyledLiveStreamPage = styled.div`

  .wrapper__wrapperBody {
    display: flex;

    .wrapper__wrapperVideoLS {
      flex-basis: 70%;
      height: 500px;
      position: relative;

      #VideoLSContainer {
        width: 100%;
        height: 100%;
        background-color: black;

        .wrapper-ls {
          width: 100%;
          height: 100%;
        }
      }

      .badge__badgeLive {
        position: absolute;
        top: 10px;
        left: 20px;
        display: flex;

        span {
          font-size: 14px;
        }
      }
    }

    .wrapper__wrapperChat {
      padding-top: 10px;
      flex-basis: 30%;
      height: 500px;
      border-right: solid 1px #d1dbe4;
    }
  }

  .wrapper__wrapperSectionLiveStream {
    .video-section {
      display: grid;
      grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
      gap: 3rem 1rem;
      padding: 3rem 0;
      margin: 0 1.5rem;
      border-top: 4px solid #CCC;
    }

    .video-section-title {
      grid-column: 1 / -1;
      margin: -.5rem 0;
      display: flex;
      justify-content: space-between;
      align-items: center;
      padding: 0 .5rem;
    }

    .video-section-title-close {
      border: none;
      background: none;
      padding: 0;
      font-size: 2rem;
      color: #555;
      cursor: pointer;
    }

    .video-section:first-child {
      border-top: none;
    }

    .video-container {
      display: flex;
      flex-direction: column;
    }

    .thumbnail {
      position: relative;
      display: flex;
    }

    .thumbnail::before {
      content: attr(data-duration);
      position: absolute;
      background-color: rgba(0, 0, 0, .85);
      color: white;
      right: 5px;
      bottom: 5px;
      padding: .1em .3em;
      border-radius: .3em;
      font-size: .9rem;
    }

    .thumbnail-image {
      width: 100%;
      height: 100%;
      min-width: 250px;
      min-height: 150px;
      background-color: #AAA;
    }

    .video-bottom-section {
      display: flex;
      align-items: flex-start;
      margin-top: 1rem;
    }

    .channel-icon {
      margin-right: .75rem;
      border-radius: 50%;
      width: 36px;
      height: 36px;
      background-color: #AAA;
    }

    .video-details {
      display: flex;
      flex-direction: column;
    }

    .video-title {
      font-size: 1.1rem;
      font-weight: bold;
      margin-bottom: .5rem;
      text-decoration: none;
      color: black;
    }

    .video-channel-name {
      margin-bottom: .1rem;
      text-decoration: none;
      transition: color 150ms;
    }

    .video-channel-name:hover {
      color: #111;
    }

    .video-channel-name,
    .video-metadata {
      color: #555;
    }
  }

  @media (max-width: 991px) {
    .wrapper__wrapperBody { 
      flex-direction: column;
    }
  }
`;