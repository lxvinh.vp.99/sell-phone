import styled from 'styled-components';

export const StyledAvatar = styled.div`
  background:
    linear-gradient(
      45deg,
      rgba(13, 33, 78, 1) 0%,
      rgba(159, 0, 73, 1) 100%
    );
  width: ${props => props.size};
  height: ${props => props.size};
  padding: ${props => props.padding};
  border-radius: 50%;

  .wrapper {
    width: 100%;
    height: 100%;
    background: white;
    overflow: hidden;
    border-radius: 50%;
    position: relative;
  }

  .wrapper img {
    position: absolute;
    width: ${props => props.imgSize};
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;

export const foo = () => { };
