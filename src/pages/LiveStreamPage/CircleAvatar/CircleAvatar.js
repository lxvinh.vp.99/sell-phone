import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyledAvatar } from './styled';

class CircleAvatar extends PureComponent {
  render() {
    const {
      src,
      size,
      padding,
    } = this.props;

    return (
      <StyledAvatar
        size={size}
        padding={padding}
        imgSize={src ? '100%' : '60%'}
      >
        <div className="wrapper">
          <img src={src} alt="avatar" />
        </div>
      </StyledAvatar>
    );
  }
}

CircleAvatar.propTypes = {
  src: PropTypes.string,
  size: PropTypes.string,
  padding: PropTypes.string,
};

CircleAvatar.defaultProps = {
  src: '',
  size: '22px',
  padding: '2px',
};

export default CircleAvatar;
