import React from 'react';
import { get } from 'lodash';
import { Modal, ModalBody, ModalFooter, Button, FormGroup, Label, Input } from 'reactstrap';
import callApi from '../../../helpers/apiCaller';

class CreateLiveModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      myProducts: [],
      selectedProduct: "",
    }
  }

  componentDidMount() {
    callApi(`products/myproducts`, 'POST', null).then(res => {
      const { data } = res.data;
      this.setState({
        myProducts: data,
        selectedProduct: get(data, '0._id'),
      });
    }).catch(err => {
      console.log(err);
    });
  }

  handleOnChange = (e) => {
    const { value } = e.target;
    this.setState({
      selectedProduct: value,
    });
  }

  handleCreateLive = () => {
    const { selectedProduct } = this.state;
    const { history, handleCloseModalCreateLive } = this.props;
    const params = {
      productId: selectedProduct,
    }
    callApi(`livestreams/create`, 'POST', params).then(res => {
      const { data } = res.data;
      history.push(`/livestreams/${data}`);
      handleCloseModalCreateLive();
    }).catch(err => {
      console.log(err);
    });
  }

  render() {
    const { myProducts, selectedProduct } = this.state;
    const { handleCloseModalCreateLive } = this.props;
    return (
      <Modal isOpen toggle={() => { }} style={{ marginTop: '50px' }}>
        <ModalBody style={{ maxHeight: '500px', overflow: 'auto' }}>
          <FormGroup>
            <Label for="exampleSelect">Chọn một sản phẩm</Label>
            <Input type="select" name="select" id="exampleSelect"
              value={selectedProduct}
              onChange={this.handleOnChange}
            >
              {
                myProducts.map((product) => {
                  return (
                    <option value={product._id}>{product.title}</option>
                  )
                })
              }
            </Input>
          </FormGroup>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.handleCreateLive}>Tạo livestream</Button>{' '}
          <Button color="secondary" onClick={() => {
            handleCloseModalCreateLive();
          }}>Hủy</Button>
        </ModalFooter>
      </Modal>
    )
  }
}

export default CreateLiveModal;