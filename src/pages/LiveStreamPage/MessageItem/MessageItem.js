import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import CircleAvatar from '../CircleAvatar/CircleAvatar';
import { StyledMessageItem } from './styled';

class MessageItem extends PureComponent {
  render() {
    const { message } = this.props;
    return (
      <StyledMessageItem type={message.type}>
        <div className="chat-item">
          <span className="text-message-wrapper">
            <span className="userinfo-wrap">
              <span className="avatar-wrap">
                <CircleAvatar src={message.avatar ? message.avatar : ''} />
              </span>
              <span className="sender">
                {message.nick ? message.nick : message.from}
                &nbsp;
              </span>
            </span>
            <span className="divider">:&nbsp;</span>
            <span className="text" id={`message__message${message.ID}`}>{_.get(message, 'payload.text')}</span>
          </span>
        </div>
      </StyledMessageItem>
    );
  }
}

MessageItem.propTypes = {
  message: PropTypes.shape([PropTypes.object]).isRequired,
};

export default MessageItem;
