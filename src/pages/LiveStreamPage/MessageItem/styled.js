import styled from 'styled-components';

export const StyledMessageItem = styled.div`
  width: 100%;
  margin-bottom: 5px;

  .chat-item {
    width: 100%;

    box-sizing: border-box;
    text-align: left;
    font-size: 10.8px;

    .text-message-wrapper {
      display: inline-block;
      ${props => props.type !== "user_send" && 'background-color: #dba200'};
      border-radius: 6px;
      padding: 3px 4px;
      word-break: break-word;

      .userinfo-wrap {
        display: inline-block;
        color: white;

        .avatar-wrap {
          display: inline-block;
          vertical-align: middle;
        }

        .sender {
          margin-left: 5px;
          vertical-align: middle;

          img {
            width: 10px;
            margin-right: 3px;
          }
        }
      }

      .divider {
        color: white;
        vertical-align: middle;
      }

      .text {
        vertical-align: middle;
        ${props => (props.type !== "user_send" ? 'color: #000' : 'color: white')};

        img {
          height: 20px;
          vertical-align: top;
        }
      }
    }
  }
`;

export const foo = () => { };
