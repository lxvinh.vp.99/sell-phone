import React from 'react';
import { get, isEmpty } from 'lodash';
import withAuth from '../../components/withAuth/withAuth';
import { connect } from 'react-redux';
import {
  ConversationHeader,
  Avatar,
} from "@chatscope/chat-ui-kit-react";
import { generateUserSig } from '../../helpers/utils';
import callApi from '../../helpers/apiCaller';
import { SDKAPPID, SECRETKEY, IM_LOG_LEVEL_NONE } from '../../constants/Config';
import { StyledLiveStreamPage } from './styled';
import Chat from './Chat';
import VideoLiveStream from './VideoLiveStream';
import LiveStreamItem from './LiveStreamItem/LiveStreamItem';
import CreateLiveModal from './CreateLiveModal/CreateLiveModal';

class LiveStreamPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      liveStreamInfo: {},
      liveStreamList: [],
      isShowModalCreateLive: false,
    };
  }

  componentDidMount() {
    this.getLiveStream();
  }

  componentDidUpdate(prevProps) {
    const { location } = this.props;
    if (location.pathname !== prevProps.location.pathname) {
      this.setState({
        liveStreamInfo: {},
        liveStreamList: [],
      }, () => {
        this.getLiveStream();
      });
    }
  }

  getLiveStream = () => {
    const { match } = this.props;
    const { id } = match.params;
    if (!id || id === 'undefined') {
      callApi(`livestreams/list`, 'POST', null).then(res => {
        const { data } = res.data;
        this.setState({
          liveStreamList: data,
        })
      }).catch(err => {
        console.log(err);
      });
    } else {
      callApi(`livestreams/detail/${id}`, 'POST', null).then(res => {
        const { data } = res.data;
        this.setState({
          liveStreamInfo: data,
        })
      }).catch(err => {
        console.log(err);
      });
    }
  }

  handleCloseModalCreateLive = () => {
    this.setState({
      isShowModalCreateLive: false,
    });
  }

  render() {
    const { authData, location, history, match } = this.props;
    const { liveStreamInfo, liveStreamList, isShowModalCreateLive } = this.state;
    return (
      <React.Fragment>
        <StyledLiveStreamPage className="container">
          {!isEmpty(liveStreamInfo) && (
            <>
              <ConversationHeader>
                <ConversationHeader.Back onClick={() => { history.push('/livestreams') }} />
                {/* <Avatar src={null} name="Joe" /> */}
                <ConversationHeader.Content userName={`LIVE: ${get(liveStreamInfo, 'product.title')}`} info={get(liveStreamInfo, 'product.sellerFullName')} />
              </ConversationHeader>
              <div className="wrapper__wrapperBody">
                <VideoLiveStream
                  authData={authData}
                  location={location}
                  history={history}
                  match={match}
                  liveStreamInfo={liveStreamInfo}
                />
                <Chat
                  authData={authData}
                  location={location}
                  history={history}
                  match={match}
                />
              </div>
            </>
          )}
          {isEmpty(liveStreamInfo) && (
            <div className="wrapper__wrapperSectionLiveStream">
              <section className="video-section">
                <h2 class="video-section-title">
                  Trực tiếp bây giờ
              </h2>
                <button onClick={() => {
                  this.setState({
                    isShowModalCreateLive: true,
                  });
                }}
                  style={{
                    minHeight: '200px'
                  }}><i className="fas fa-plus" /></button>
                {liveStreamList.map((live) => {
                  return (
                    <LiveStreamItem
                      key={live._id}
                      liveInfo={live}
                    />
                  )
                })}
              </section>
            </div>
          )}
          {isShowModalCreateLive && (
            <CreateLiveModal
              handleCloseModalCreateLive={this.handleCloseModalCreateLive}
              history={history}
            />)}
        </StyledLiveStreamPage>
      </React.Fragment >
    )
  }
}

const mapStateToProps = state => ({
  authData: state.auth,
});

export default withAuth(connect(mapStateToProps, null)(LiveStreamPage));
