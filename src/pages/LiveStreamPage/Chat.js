import React from 'react';
import TIM from 'tim-js-sdk';
import { get } from 'lodash';
import "@chatscope/chat-ui-kit-styles/dist/default/styles.min.css";
import {
  ChatContainer,
  MessageList,
  MessageInput,
} from "@chatscope/chat-ui-kit-react";
import { generateUserSig } from '../../helpers/utils';
import { SDKAPPID, SECRETKEY, IM_LOG_LEVEL_NONE } from '../../constants/Config';
import MessageItem from './MessageItem/MessageItem';

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messageList: [],
      chatText: '',
      isSDKReady: false,
    };
    const options = {
      SDKAppID: SDKAPPID,
    };
    this.tim = TIM.create(options);
    this.tim.setLogLevel(IM_LOG_LEVEL_NONE);
    this.subscribeEventIM();
  }

  async componentDidMount() {
    const { authData } = this.props;
    if (!authData.isLogin) return;
    const userID = get(authData, 'userInfo.userId').toString();
    try {
      const userSig = generateUserSig(SDKAPPID, SECRETKEY, userID);
      await this.tim.login({ userID, userSig });
    } catch (error) {
      console.log(error);
    }
  }

  joinGroupIM = async () => {
    const { match } = this.props;
    const groupID = match.params.id;
    const imRes = await this.tim.joinGroup({ groupID: groupID, type: TIM.TYPES.GRP_AVCHATROOM });
  }

  handleChangeInput = (value) => {
    this.setState({
      chatText: value,
    });
  }

  subscribeEventIM = () => {
    this.tim.on(TIM.EVENT.SDK_READY, this.onSDKReady);
    this.tim.on(TIM.EVENT.SDK_NOT_READY, this.onSDKNotReady);
    this.tim.on(TIM.EVENT.MESSAGE_RECEIVED, this.onMessageReceived);
  }

  cancelEventIM = () => {
    this.tim.off(TIM.EVENT.SDK_READY, this.onSDKReady);
    this.tim.off(TIM.EVENT.SDK_NOT_READY, this.onSDKNotReady);
    this.tim.off(TIM.EVENT.MESSAGE_RECEIVED, this.onMessageReceived);
  }

  onSDKReady = async () => {
    console.log('SDK ready to send message');
    this.setState({
      isSDKReady: true,
    });
    this.joinGroupIM();
  }

  onSDKNotReady = () => {
    this.setState({
      isSDKReady: false,
    });
    console.log('SDK not ready, please try again');
  }

  onMessageReceived = (evt) => {
    const { messageList } = this.state;
    evt.data.forEach((message) => {
      if (message.type === TIM.TYPES.MSG_TEXT) {
        messageList.push(message);
      }
    });
    this.setState({
      messageList: [...messageList],
    });
  }

  sendMessage = async () => {
    const { match } = this.props;
    const { id: groupID } = match.params;
    const { chatText, messageList } = this.state;
    let message = this.tim.createTextMessage({
      to: groupID,
      conversationType: TIM.TYPES.CONV_GROUP,
      payload: {
        text: chatText,
      },
    });
    try {
      const imRes = await this.tim.sendMessage(message);
      const { message: sendMessage } = imRes.data;
      this.setState({
        chatText: '',
        messageList: [...messageList, sendMessage],
      });
    } catch (error) {
      console.log(error);
    }
  }

  renderMessageList = () => {
    const { messageList } = this.state;
    return (<MessageList>
      {messageList.map((message) => {
        return message && <MessageItem message={message} key={message.id} />;
      })}
    </MessageList>);
  }

  renderConversationHeader = () => {
    return null;
  }

  render() {
    const { isSDKReady, chatText } = this.state;

    return (
      <ChatContainer className="wrapper__wrapperChat">
        {this.renderConversationHeader()}
        {this.renderMessageList()}
        <MessageInput
          value={chatText}
          onChange={this.handleChangeInput}
          attachButton={false}
          placeholder="Viết tin nhắn..."
          disabled={!isSDKReady}
          onSend={this.sendMessage}
        />
      </ChatContainer>
    )
  }
}

export default Chat;