import React, { useState } from 'react';
import withAuth from '../../components/withAuth/withAuth';
import ManageProductsFrame from '../../components/ManageProductsFrame/ManageProductsFrame';

const ManageProductsPage = () => {
    return (
        <React.Fragment>
            <div className="container">
                <div className="text-center">
                    <h1>Quản lý sản phẩm đăng bán</h1><hr />
                </div>
                <ManageProductsFrame></ManageProductsFrame>
            </div>
        </React.Fragment>
    )
}
export default withAuth(ManageProductsPage);
