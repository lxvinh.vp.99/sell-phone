import React from 'react';
import withAuth from '../../components/withAuth/withAuth';
import './HomePage.scss';
import ProductFrame from '../../components/ProductFrameHome/ProductFrame';
import { ToastContainer, Slide } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { StyledHomePage } from './styled';
import CategoriesHomePage from '../../components/CategoriesHomePage/CategoriesHomePage';
const Header = (props) => {
  return (
    <React.Fragment>
      <ToastContainer
        style={{ top: 56 + "px" }}
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        transition={Slide}
      />

      <StyledHomePage>
        <div class="Home__WrapperHome">
          <CategoriesHomePage />
          <ProductFrame {...props}></ProductFrame>
        </div>
      </StyledHomePage>
    </React.Fragment>
  )
}
export default withAuth(Header);
