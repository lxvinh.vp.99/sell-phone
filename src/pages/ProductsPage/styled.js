import styled from 'styled-components';

export const StyledHomePage = styled.main`
    background-image: url(http://static.chotot.com/storage/banner/Theming-1920x1080_v4.jpg);
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-size: cover;
    width: 100%;
    height: 100%;

    .Home__WrapperHome {
      margin: 0px auto;
      max-width: 960px;
    }

    .Categories__Wrapper {
      background: rgb(255, 255, 255);
      padding: 12px 12px 4px;

      .Wrapper__WrapperCategories {
        overflow-x: auto;
        background-color: #fff;
        display: grid;
        grid-template-rows: 1fr 1fr;
        grid-auto-flow: column;
        list-style: none;

        li {
          height: 140px;
          outline: none;
          cursor: pointer;

          a {
            display: flex;
            align-items: center;
            width: 135px;
            text-decoration: none;
            flex-direction: column;

            img {
              width: 84px;
              height: 84px;
              user-select: none;
              border-radius: 20px;
              outline: none;
            }

            span {
              color: rgb(34, 34, 34);
              text-shadow: rgb(255 255 255) 0px 0px;
              line-height: 1.29;
              font-size: 14px;
              text-align: center;
              width: 130px;
              height: 16px;
              margin-top: 8px;
              font-weight: normal;
              font-stretch: normal;
              font-style: normal;
            }
          }
        }
      }
    }

    .Categories__Wrapper .Categories__Title {
      font-size: 17px;
      margin: 0px 0px 12px;
      font-weight: bold;
      font-stretch: normal;
      font-style: normal;
      line-height: 1.41;
      letter-spacing: normal;
      color: rgb(34, 34, 34);
    }

    .Home__WrapperHome .GridAds__Wrapper {
      background: rgb(255, 255, 255);
    }

    .Home__WrapperHome .GridAds__Wrapper .GridAds__Title {
      font-size: 17px;
      font-weight: bold;
      margin: 0px;
      padding: 12px;
      font-stretch: normal;
      font-style: normal;
      line-height: 1.41;
      letter-spacing: normal;
      color: rgb(34, 34, 34);
    }

    .Home__WrapperHome .GridAds__Wrapper .GridAds__Grid {
      display: flex;
      flex-wrap: wrap;
      grid-template-columns: repeat(5, 1fr);
      border-top: 1px solid rgb(244, 244, 244);
    }

    .Home__WrapperHome
    .GridAds__Wrapper
    .GridAds__Grid
    .AdItem__Item {
      border-bottom: 1px solid rgb(244, 244, 244);
      border-right: 1px solid rgb(244, 244, 244);
      padding: 12px;
      flex-basis: 20%;
    }

    .Home__WrapperHome
    .GridAds__Wrapper
    .GridAds__Grid
    .AdItem__Item:hover {
      box-shadow: 0 1px 7px 0 rgb(0 0 0 / 30%);
      z-index: 2;
    }

    .Home__WrapperHome
    .GridAds__Wrapper
    .wrapper__wrapperBreadcrumb {
      margin-bottom: 0;
      border-radius: 0;
    }

    .Home__WrapperHome
    .GridAds__Wrapper
    .wrapper__wrapperFilter {
      padding: 10px;

      .wrapper__wrapperButtonFilter {
        margin-right: 10px;

        button {
          background-color: white;
          color: black;
        }
      }
    }

    .wrapperLink {
      position: relative;
      display: block;
      height: 100%;
    }

    .wrapperLink .thumbnailWrapper {
      position: relative;
      display: flex;
      justify-content: center;
    }

    .wrapperLink .thumbnailWrapper .thumbnailImg {
      position: relative;
      background: url(https://static.chotot.com/storage/default_images/c2c_ad_image.jpg)
        50% no-repeat;
      background-size: cover !important;
      width: 100%;
      border-radius: 2px;
      overflow: hidden;
      display: inline-block;
    }

    .wrapperLink .thumbnailWrapper .thumbnailImg::after {
      display: block;
      padding-top: 100%;
      content: "";
    }

    .wrapperLink .thumbnailWrapper .thumbnailImg img {
      position: absolute;
      width: 100%;
      height: 100%;
      object-fit: cover;
    }

    .wrapperLink .thumbnailWrapper .saveAdWrapper {
      position: absolute;
      right: 8px;
      bottom: 4px;
      z-index: 9;
    }

    .wrapperLink .thumbnailWrapper .saveAdWrapper .btnSavedAd {
      padding: 0;
      background: transparent;
      cursor: pointer;
      border: none;
      outline: none;
    }

    .wrapperLink .caption {
      padding-bottom: 8px;
      position: relative;
    }

    .caption .adTitleGrid {
      color: #222;
      font-size: 14px !important;
      line-height: 1.43 !important;
      text-overflow: unset;
      text-decoration: none;
      white-space: unset;
      margin-top: 8px;
      height: 40px !important;
      display: block;
      overflow: hidden;
    }

    .caption .price {
      font-size: 15px;
      font-weight: 700;
      line-height: 1.33;
      color: #d0021b;
      display: inline-block;
      margin: 2px 4px 0 0;
    }

    .wrapperLink .footerItem {
      color: #9b9b9b;
      bottom: -6px;
      width: 100%;
      left: 0;
      display: flex;
    }

    .footerItem a {
      background-color: transparent;
    }

    .footerItem a img {
      margin-top: 1px;
      margin-right: 3px;
      vertical-align: middle;
    }

    .deviderWrapper {
      display: inline-block;
    }

    .deviderWrapper::after {
      vertical-align: middle;
      content: '•';
      display: inline-block;
    }

    .footerItem .adItemPostedTime {
      vertical-align: middle;
      display: inline-block;
      font-size: 11px;
      overflow: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
      text-transform: capitalize;
      margin-top: 2px;
    }

    .footerItem .adItemPostedTime .text {
      font-size: 10px;
      margin: 0 3px;
    }

    .footerItem .adItemPostedTime.location .text {
      margin-right: 0;
    }

    @media screen and (min-width: 768px) {
      .Home__HomeContainer
        .Home__WrapperHome
        .GridAds__Wrapper
        .GridAds__Grid
        .AdItem__Item:nth-child(5n) {
        border-right: none;
      }
    }

    /* width <= 768 */
    @media screen and (max-width: 768px) {
      .Home__HomeContainer
        .Home__WrapperHome
        .GridAds__Wrapper
        .GridAds__Grid
        .AdItem__Item {
        flex-basis: 25%;
      }
    }

    @media screen and (max-width: 540px) {
      .Home__HomeContainer
        .Home__WrapperHome
        .GridAds__Wrapper
        .GridAds__Grid
        .AdItem__Item {
        flex-basis: 50%;
      }
    }
`;