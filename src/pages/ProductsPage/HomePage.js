import React from 'react';
import withAuth from '../../components/withAuth/withAuth';
import './HomePage.scss';
import ProductFrame from '../../components/ProductFrame/ProductFrame';
import { ToastContainer, Slide } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { StyledHomePage } from './styled';
const Header = (props) => {
  return (
    <React.Fragment>
      <ToastContainer
        style={{ top: 56 + "px" }}
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        transition={Slide}
      />

      <StyledHomePage>
        <div class="Home__WrapperHome">
          <ProductFrame {...props}></ProductFrame>
        </div>
      </StyledHomePage>
    </React.Fragment>
  )
}
export default withAuth(Header);
